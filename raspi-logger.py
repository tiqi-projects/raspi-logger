#!/usr/bin/env python

# RaspberryPI Logger (to InfluxDB on gallium.ethz.ch)
#
# * RaspberryPI Pin Mapping:
#   http://makezine.com/projects/tutorial-raspberry-pi-gpio-pins-and-python/
#
# * Arduino (optionally):
#   (reads Arduino analog ports through firmata if connected and uploaded)
#   http://www.arduino.cc/en/pmwiki.php?n=Main/ArduinoBoardUno
#
# (thanks to Alex started from https://bitbucket.org/tiqitaka/reptoar-loggerclient/src/68443c3f6220c11a1ca7046d18562c60be4c8a1d/logger08.py?at=master)
#
# Setup Software:
#   
#   $ sudo apt-get install python-pip
#   $ sudo pip install influxdb


import RPi.GPIO as GPIO
try:
    from pyfirmata import Arduino, util
except:
    pass

from influxdb.influxdb08 import InfluxDBClient

from uuid import getnode as get_mac

#import zmq, msgpack

import time


__logger_id__ = hex(get_mac())            # change if you want, default is the mac address
__author__    = 'Zievi Ursin Soler'
__email__     = 'usoler@phys.ethz.ch'


#GPIO.setmode(GPIO.BOARD)
GPIO.setmode(GPIO.BCM)
try:
    board = Arduino('/dev/ttyUSB0')
except:
    board = None

client08 = InfluxDBClient('gallium.ethz.ch', 8086, 'tiqidb', 'tiqi', 'tiqi')

cols = ['pin%02i' % pin for pin in range(40)]

for pin in range(40):
    func = GPIO.gpio_function(pin)
    print pin, func

    #GPIO.setup(pin, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)
    GPIO.setup(pin, GPIO.IN)

if board:
    # https://github.com/tino/pyFirmata/blob/master/pyfirmata/util.py#L42
    it = util.Iterator(board)
    it.start()                # receive Arduino messages in background (2nd thread)
    for pin in range(6):
        board.analog[pin].enable_reporting()

# poll pin status and write them to the influx database
while True:
    digital_in = [None] * 40
    for pin in range(40):
#        func = GPIO.gpio_function(pin)
#        print pin, func
        digital_in[pin] = GPIO.input(pin)

    analog_in = [None] * 6
    if board:
        for pin in range(6):
            analog_in[pin] = board.analog[pin].read()

    # write to influx 0.8 on gallium
    points08 = [
        {
            'name': 'raspi-log-%s-digital' % __logger_id__,
#            "tags": {
#                "host": "server01",
#                "region": "us-west"
#            },
#            "timestamp": "2009-11-10T23:00:00Z",
            'columns': cols,
            'points': [digital_in]
#            "fields": {
#                "value": 0.64
#        }
        }, {
            'name': 'raspi-log-%s-analog' % __logger_id__,
            'columns': cols[:6],
            'points': [analog_in]
        },
    ]
    try:
        client08.write_points(points08)
    except Exception as e:
        print(e)

    time.sleep(10.)

GPIO.cleanup()
if board:
    for pin in range(6):
        board.analog[pin].disable_reporting()
    #it.stop()
    board.exit()
